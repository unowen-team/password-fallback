<?php

if (!defined('PASSWORD_DEFAULT')) {

    define('PASSWORD_BCRYPT', 1);
    define('PASSWORD_DEFAULT', PASSWORD_BCRYPT);

    function password_hash($password, $algo, array $options = array())
    {
        return hash('sha1', $password, false);
    }

    function password_needs_rehash($hash, $algo, array $options = array())
    {
        return false;
    }

    function password_verify($password, $hash)
    {
        $ret = hash('sha1', $password, false);

        $status = 0;
        for ($i = 0; $i < strlen($ret); $i++) {
            $status |= (ord($ret[$i]) ^ ord($hash[$i]));
        }

        return $status === 0;
    }
}



